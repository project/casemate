; Drush Make file for the Casemate distribution
core = 7.x
api = 2

; Modules
; -------

; febbraro
projects[admin][subdir] = contrib
projects[admin][version] = 2.x-dev

; febbraro
projects[boxes][subdir] = contrib
projects[boxes][version] = 1.0

; febbraro
projects[casetracker][subdir] = contrib
projects[casetracker][version] = 1.x-dev

; yched
projects[cck][subdir] = contrib
projects[cck][version] = 2.x-dev

; cam8001
projects[codefilter][subdir] = contrib
projects[codefilter][version] = 1.0

; netaustin
; http://drupal.org/node/1545922
;projects[comment_upload][subdir] = contrib
;projects[comment_upload][version] =

; fago
projects[profile2][subdir] = contrib
projects[profile2][version] = 1.2

; febbraro
projects[context][subdir] = contrib
projects[context][version] = 3.0-beta4

; yhahn
;projects[crayon][subdir] = contrib
;projects[crayon][version] =

; merlinofchaos
projects[ctools][subdir] = contrib
projects[ctools][version] = 1.2

; febbraro
projects[data][subdir] = contrib
projects[data][version] = 1.x-dev

; KarenS
projects[date][subdir] = contrib
projects[date][version] = 2.6

; yhahn
projects[designkit][subdir] = contrib
projects[designkit][version] = 1.0-beta1

; yhahn
projects[diff][subdir] = contrib
projects[diff][version] = 3.1

; fago
projects[entity][subdir] = contrib
projects[entity][version] = 1.0-rc3

; Damien Tournoud
projects[entityreference][subdir] = contrib
projects[entityreference][version] = 1.0

; mpotter
projects[features][subdir] = contrib
projects[features][version] = 1.0

; febbraro
projects[feeds][subdir] = contrib
projects[feeds][version] = 2.0-alpha7

; v1nce
projects[imagecache_profiles][subdir] = contrib
projects[imagecache_profiles][version] = 1.x

; iva2k
;projects[itweak_upload][subdir] = contrib
;projects[itweak_upload][version] =

; febbraro
;projects[job_scheduler][subdir] = contrib
;projects[job_scheduler][version] =

; jjeff
projects[jquery_update][subdir] = contrib
projects[jquery_update][version] = 2.2

; sun
projects[libraries][subdir] = contrib
projects[libraries][version] = 2.0

; febbraro
;projects[litecal][subdir] = contrib
;projects[litecal][version] =

; Dane Powell
projects[mailcomment][subdir] = contrib
projects[mailcomment][version] = 2.2

; Dane Powell
projects[mailhandler][subdir] = contrib
projects[mailhandler][version] = 2.5

; justin2pin
projects[markdown][subdir] = contrib
projects[markdown][version] = 1.0

; febbraro
projects[messaging][subdir] = contrib
projects[messaging][version] = 1.x-dev

; Hugo Wetterberg
projects[nodeformcols][subdir] = contrib
projects[nodeformcols][version] = 1.x-dev

; febbraro
projects[notifications][subdir] = contrib
projects[notifications][version] = 1.x-dev

; febbraro
projects[notifications_team][subdir] = contrib
projects[notifications_team][version] = 3.x-dev

; Amitaibu
projects[og][subdir] = contrib
projects[og][version] = 2.0-beta2

; Amitaibu
projects[og_views][subdir] = contrib
projects[og_views][version] = 1.0

; febbraro
projects[openidadmin][subdir] = contrib
projects[openidadmin][version] = 1.0

; ekes
;projects[parser_ical][subdir] = contrib
;projects[parser_ical][version] =

; jbrauer
projects[prepopulate][subdir] = contrib
projects[prepopulate][version] = 2.x-dev

; febbraro
projects[purl][subdir] = contrib
projects[purl][version] = 1.x-dev

; yhahn
projects[reldate][subdir] = contrib
projects[reldate][version] = 1.x-dev

; febbraro
projects[spaces][subdir] = contrib
projects[spaces][version] = 3.x-dev

; febbraro
projects[strongarm][subdir] = contrib
projects[strongarm][version] = 2.0

; eaton
projects[token][subdir] = contrib
projects[token][version] = 1.4

; smk-ka
projects[transliteration][subdir] = contrib
projects[transliteration][version] = 3.1

; febbraro
;projects[ucreate][subdir] = contrib
;projects[ucreate][version] =

; merlinofchaos
projects[views][subdir] = contrib
projects[views][version] = 3.5

; febbraro
;projects[xref][subdir] = contrib
;projects[xref][version] =

; Development modules
; -------------------

; douggreen
projects[coder][subdir] = developer
projects[coder][version] = 1.2

; moshe weitzman
projects[devel][subdir] = developer
projects[devel][version] = 1.3

; colan
projects[schema][subdir] = developer
projects[schema][version] = 1.0-beta4

; l10n
; ----

; Gábor Hojtsy
projects[l10n_client][subdir] = l10n
projects[l10n_client][version] = 1.1

; Sutharsan
projects[l10n_update][subdir] = l10n
projects[l10n_update][version] = 1.x-dev

; Themes
; ------

; febbraro
projects[tao][version] = 3.x-dev

; febbraro
projects[rubik][version] = 4.x-dev

; febbraro
;projects[ginkgo][version] =

; Libraries
; ---------
libraries[profiler][download][type] = git
libraries[profiler][download][url] = http://git.drupal.org/project/profiler.git
libraries[profiler][download][tag] = 7.x-2.0-beta1


; Include Guardr
; --------------
includes[guardr] = "http://drupalcode.org/project/guardr.git/blob_plain/refs/heads/7.x-1.x:/drupal-org.make"
